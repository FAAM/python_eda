# Estructura de datos y algoritmo con Python

<a href="https://faam.gitlab.io/python_eda/"><img alt="Link a la Documentación" src="https://img.shields.io/badge/jupyter--book-link-brightgreen"></a>
[![pipeline status](https://gitlab.com/FAAM/python_eda/badges/master/pipeline.svg)](https://gitlab.com/FAAM/python_eda/-/commits/master)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/FAAM%2Fpython_eda/HEAD)


## Contenidos temáticos

- Introduction to algorithms
- Selection sort
- Recursion
- Quicksort
- Hash tables
- Breadth-first search
- Dijkstra’s algorithm
- Dynamic programming
- K-nearest neighbors





